import ItemService from '~/assets/js/services/ItemService.js'
import CategoryService from '~/assets/js/services/CategoryService.js'
import SubcategoryService from '~/assets/js/services/SubcategoryService.js'
import BrandService from '~/assets/js/services/BrandService.js'

export default {
  data: () => ({
    itemList: [],
    brandList: [],
    categoryList: [],
    subcategoryList: [],
    relatedSubcategories: [],
    selected_item: null,
    item: {
      item_id: null,
      item_name: null,
      item_description: null,
      item_price: null,
      brand_id: null,
      subcategory_id: null
    },
    storeDialog: false,
    updateDialog: false,
    deleteDialog: false
  }),
  methods: {
    async getItems () {
      const itemService = new ItemService(this)
      const response = await itemService.index()
      if (response.data.status === 200) {
        this.itemList = response.data.items
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
    },
    async getBrands () {
      const brandService = new BrandService(this)
      const response = await brandService.index()
      if (response.data.status === 200) {
        this.brandList = response.data.brands
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
    },
    async getCategories () {
      const categoryService = new CategoryService(this)
      const response = await categoryService.index()
      if (response.data.status === 200) {
        this.categoryList = response.data.categories
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
    },
    async getSubcategories () {
      const subcategoryService = new SubcategoryService(this)
      const response = await subcategoryService.index()
      if (response.data.status === 200) {
        this.subcategoryList = response.data.subcategories
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
    },
    storeHandler () {
      this.storeDialog = true
    },
    async storeItem () {
      const itemService = new ItemService(this)
      const response = await itemService.store(this.item)
      if (response.data.status === 201) {
        this.itemList.push(response.data.item)
        this.storeDialog = false
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
    },
    handlerUpdate (_data) {
      this.item.item_id = _data.item_id
      this.item.item_name = _data.item_name
      this.item.item_description = _data.item_description
      this.item.item_price = _data.item_price
      this.item.brand_id = _data.brand_id
      this.item.subcategory_id = _data.subcategory_id
      this.updateDialog = true
    },
    async updateItem () {
      const itemService = new ItemService(this)
      const response = await itemService.update(this.item, this.item.item_id)
      if (response.data.status === 201) {
        for (let i = 0; i < this.itemList.length; i++) {
          if (this.itemList[i].item_id === this.item.item_id) {
            this.itemList.splice(i, 1, response.data.item)
            break
          }
        }
        this.updateDialog = false
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
      console.log(response)
    },
    handlerDelete (_data) {
      this.selected_item = _data.item_id
      this.deleteDialog = true
      console.log(_data)
    },
    async deleteItem () {
      const itemService = new ItemService(this)
      const response = await itemService.delete(this.selected_item)
      if (response.status === 201) {
        console.log('Entra if')
        for (let i = 0; i < this.itemList.length; i++) {
          if (this.itemList[i].item_id === this.selected_item) {
            this.itemList.splice(i, 1)
            break
          }
        }
        this.deleteDialog = false
      } else {
        this.$notify({
          title: 'Error',
          message: 'Ocurrio un error inespedado intente de nuevo',
          type: 'info'
        })
      }
    }
  },
  async mounted () {
    await this.getItems()
    await this.getBrands()
    await this.getCategories()
    await this.getSubcategories()
  }
}
