'use strict'

import ENV from '~/env.js'

export default class SubcategoryService {
  //
  constructor (scope) {
    this.scope = scope
  }

  async index () {
    const response = await this.scope.$axios.get(`${ENV.API}/subcategory`).catch(e => e.response)
    return response
  }
}
