'use strict'

import ENV from '~/env.js'

export default class CategoryService {
  //
  constructor (scope) {
    this.scope = scope
  }

  async index () {
    const response = await this.scope.$axios.get(`${ENV.API}/category`).catch(e => e.response)
    return response
  }
}
