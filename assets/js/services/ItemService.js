'use strict'

import ENV from '~/env.js'

export default class ItemService {
  //
  constructor (scope) {
    this.scope = scope
  }

  async index () {
    const response = await this.scope.$axios.get(`${ENV.API}/item`).catch(e => e.response)
    return response
  }

  // async store (_data, _id) {
  //   const response = await this.scope.$axios.post(`${ENV.API}/item`, _data).catch(e => e.response)
  //   return response
  // }

  async store (_data) {
    const parse = new URLSearchParams(_data).toString()
    const response = await this.scope.$axios.post(`${ENV.API}/item?` + parse).catch(e => e.response)
    return response
  }

  async update (_data, _id) {
    const response = await this.scope.$axios.put(`${ENV.API}/item/${_id}`, _data).catch(e => e.response)
    return response
  }

  async delete (_id) {
    const response = await this.scope.$axios.delete(`${ENV.API}/item/${_id}`).catch(e => e.response)
    return response
  }
}
