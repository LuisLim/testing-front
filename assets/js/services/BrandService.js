'use strict'

import ENV from '~/env.js'

export default class BrandService {
  //
  constructor (scope) {
    this.scope = scope
  }

  async index () {
    const response = await this.scope.$axios.get(`${ENV.API}/brand`).catch(e => e.response)
    return response
  }
}
